package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/Hassanhs97/data-processing/handlers"
)

func main() {
	r := gin.Default()

	
	r.POST("/process-data", handlers.CreateData)

	port := os.Getenv("PORT")
	http.ListenAndServe(":" + port, r)
}