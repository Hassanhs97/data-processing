FROM golang:latest

WORKDIR /app

COPY . .

RUN go build -o main .

EXPOSE 8088


CMD ["./main"]